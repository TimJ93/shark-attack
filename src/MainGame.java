import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import model.Model;


public class MainGame extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Model model = new Model();
        Canvas canvas = new Canvas(model.WIDTH, model.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);

        Scene scene = new Scene(group);

        stage.setScene(scene);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(gc, model);

        Timer timer = new Timer(graphics, model);
        timer.start();

        InputHandler inputHandler = new InputHandler(model);
        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );
        scene.setOnKeyReleased(event -> inputHandler.onKeyReleased(event.getCode()));

        Thread gamepadController = new GamepadController(model);
        gamepadController.start();

        stage.show();
        stage.setOnCloseRequest(windowEvent -> {
            gamepadController.stop();
            stage.close();
        });
    }
}