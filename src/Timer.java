import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {
        if (!model.isFinished()) {
            model.update();
        }
        graphics.draw();
    }
}
