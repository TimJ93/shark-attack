import model.Model;
import net.java.games.input.*;

import java.util.Arrays;
import java.util.List;

public class GamepadController extends Thread {
    private Model model;

    public GamepadController(Model model) {
        this.model = model;
    }

    @Override
    public void run() {
        //Liste aller Controller
        List<Controller> gamepads = Arrays.asList(ControllerEnvironment.getDefaultEnvironment().getControllers());
        //Gamepad an Stelle 6
        Controller gamepad = gamepads.get(gamepads.size() - 1);

        Event event;
        Component component;
        float value;
        String tempPosition = "";

        while (true) {
            //is the controller available?
            gamepad.poll();
            //Queue with all events
            EventQueue eq = gamepad.getEventQueue();
            event = new Event();
            //iteration through events
            while (eq.getNextEvent(event)) {
                component = event.getComponent();
                value = event.getValue();

                if (component.isAnalog()) {
                    // clear temporarily stored position if analog stick is in neutral position
                    if ((value < 0.3) && (value > -0.3) && (tempPosition.equals(component.getIdentifier().getName()))) {
                        tempPosition = "";
                        model.getPlayer().setDy(0);
                        model.getPlayer().setDx(0);
                    }
                    // input from analog-sticks and back triggers
                    if ((value > 0.8) && !(tempPosition.equals(component.getIdentifier().getName()))) {
                        // positive direction
                        switch (component.getIdentifier().getName()) {
                            case "x":
                                // left stick - RIGHT
                                tempPosition = "x";
                                model.getPlayer().setDx(2);
                                break;
                            case "y":
                                // left stick - DOWN
                                tempPosition = "y";
                                model.getPlayer().setDy(2);

                                break;
                            case "rx":
                                // right stick - RIGHT
                                tempPosition = "rx";

                                break;
                            case "ry":
                                // right stick - DOWN
                                tempPosition = "ry";

                                break;
                            case "z":
                                // left trigger (z-axis)
                                tempPosition = "z";

                                break;
                        }
                    }

                    if (value < -0.8 && !(tempPosition.equals(component.getIdentifier().getName()))) {
                        // negative direction
                        switch (component.getIdentifier().getName()) {
                            case "x":
                                // left stick - LEFT
                                tempPosition = "x";
                                model.getPlayer().setDx(-2);

                                break;
                            case "y":
                                // left stick - UP
                                tempPosition = "y";
                                model.getPlayer().setDy(-2);

                                break;
                            case "rx":
                                // right stick - LEFT
                                tempPosition = "rx";

                                break;
                            case "ry":
                                // right stick - UP
                                tempPosition = "ry";

                                break;
                            case "z":
                                // right trigger (z-axis)
                                tempPosition = "z";
                                System.out.println("trigger");

                                break;
                        }
                    }
                } else {
                    // clear temporarily stored position if button is in neutral position
                    if (value == 0) {
                        model.getPlayer().setDx(model.getPlayer().getDx() / 3);
                        model.getPlayer().setDy(model.getPlayer().getDy() / 3);
                        model.getPlayer().setSprinting(false);
                    }
                    // input from buttons, dpad analog-stick-pushes
                    if (value == 1.0) {
                        switch (component.getIdentifier().getName()) {

                            case "1":
                                // B-Button
                                model.reset();
                                break;

                            case "5":
                                // RB-Button
                                model.getPlayer().setDx(model.getPlayer().getDx() * 3);
                                model.getPlayer().setDy(model.getPlayer().getDy() * 3);
                                model.getPlayer().setSprinting(true);

                                break;
                            case "6":
                                // Back-Button

                                break;
                            case "7":
                                // Start-Button

                                break;
                            case "10":
                                // Left Stick Push


                                break;
                            case "9":
                                // Right Stick Push

                                break;
                            case "pov":
                                // DPad Left

                                break;
                            default:
                                break;
                        }
                    } else if (value == 0.25) {
                        // DPad Up

                    } else if (value == 0.5) {
                        // DPad Right

                    } else if (value == 0.75) {
                        // DPad Down

                    }

                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}

