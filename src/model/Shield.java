package model;

public class Shield extends NPC {

    public Shield(int x, int y) {
        super(x, y);
        this.h = 90;
        this.w = 90;
        this.dx = 0;
        this.dy = 0;
    }

    @Override
    public void update() {
        if (this.h < 90 && this.w < 90) {
            this.h += 1;
            this.w += 1;
        } else {
            this.h = 60;
            this.w = 60;
        }
    }
}
