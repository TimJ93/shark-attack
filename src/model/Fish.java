package model;

public class Fish extends NPC {
    public Fish(int x, int y) {
        super(x, y);
        this.w = 50;
        this.h = 50;
        this.dx = 1;
        this.dy = 1;
    }

    public void update() {
        if (Math.random() < 0.009) {
            dx = (int) (Math.random() * 10 - 5);
            dy = (int) (Math.random() * 10 - 5);

        }
        if (x + dx > Model.WIDTH - w || x + dx < 0) {
            dx *= -1;
        }
        if (y + dy > Model.HEIGHT - h || y + dy < 0) {
            dy *= -1;
        }

        if (x + dx <= Model.WIDTH - w && x + dx >= 0 &&
                y + dy <= Model.HEIGHT - h && y + dy >= 0) {
            move();
        }
    }
}
