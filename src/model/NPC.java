package model;

public abstract class NPC {
    //coordinates of npc
    protected int x, y;
    //height and width of npc
    protected int h, w;

    protected float dx, dy;

    public NPC(int x, int y) {
        this.x = x;
        this.y = y;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public float getDx() {
        return dx;
    }

    public float getDy() {
        return dy;
    }

    public void move() {
        this.x += dx;
        this.y += dy;
    }

    public abstract void update();

}
