package model;

public class Diver extends NPC {

    public Diver(int x, int y) {
        super(x, y);
        this.h = 90;
        this.w = 130;
        this.dx = 1;
        this.dy = 1;
    }

    @Override
    public void update() {
        if (Math.random() < 0.009) {
            dx = (int) (Math.random() * 6 - 3);
            dy = (int) (Math.random() * 6 - 3);

        }
        if (x + dx > Model.WIDTH - w || x + dx < 0) {
            dx *= -1;
        }
        if (y + dy > Model.HEIGHT - h || y + dy < 0) {
            dy *= -1;
        }

        if (x + dx <= Model.WIDTH - w && x + dx >= 0 &&
                y + dy <= Model.HEIGHT - h && y + dy >= 0) {
            move();
        }
    }
}
