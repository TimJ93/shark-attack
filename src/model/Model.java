package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Model {

    public static final int WIDTH = 1400;
    public static final int HEIGHT = 800;
    private Player player;
    private List<NPC> npcs = new LinkedList<>();
    private boolean finished;
    private int score;
    private BufferedReader reader;
    private FileWriter writer;
    private int highscore;

    //reading the Highscore from txt file
    {
        try {
            reader = new BufferedReader(new FileReader("src/HighScore.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            highscore = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public Model() {
        this.player = new Player(500, 400);
        finished = false;
        score = 0;
        npcs.add(new Fish(0, 0));
        npcs.add(new Fish(WIDTH, HEIGHT / 2));

    }

    public Player getPlayer() {
        return player;
    }

    public List<NPC> getNpcs() {
        return npcs;
    }

    public boolean isFinished() {
        return finished;
    }

    public int getScore() {
        return score;
    }

    public int getHighscore() {
        return highscore;
    }

    //resetting the model for restart
    public void reset() {
        this.player = new Player(500, 400);
        finished = false;
        score = 0;
        npcs.clear();
        npcs.add(new Fish(0, 0));
        npcs.add(new Fish(WIDTH, HEIGHT / 2));
        {
            try {
                reader = new BufferedReader(new FileReader("src/HighScore.txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                highscore = Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void update() {
        //movement just possible in the borders of the scene
        if (player.getY() + player.getDy() >= 0 && player.getY() + player.getDy() <= HEIGHT - player.getH() &&
                player.getX() + player.getDx() >= 0 && player.getX() + player.getDx() <= WIDTH - player.getW()
                && player.getStamina() > 0) {
            player.move();
        }
        //stamina regenerates automatically
        player.regenerate();

        //movement of npcs
        if (!npcs.isEmpty()) {
            for (NPC npc : npcs) {
                npc.update();
            }
        }
        //player's health decreases over time
        player.updateLife();

        //Spawns
        if (Math.random() < 0.0006) {
            boolean left = Math.random() > 0.5;
            if (left) {
                npcs.add(new UBoot(0, (int) (Math.random() * 700)));
            } else {
                npcs.add(new UBoot(WIDTH - 200, (int) (Math.random() * 700)));
            }
        }
        if (Math.random() < 0.0004) {
            npcs.add(new Shield((int) (Math.random() * (Model.WIDTH - 120)), (int) (Math.random() * (Model.HEIGHT - 120))));
        }
        if (Math.random() < 0.005) {
            boolean left = Math.random() > 0.5;
            if (left) {
                npcs.add(new Fish(0, (int) (Math.random() * 700)));
            } else {
                npcs.add(new Fish(WIDTH - 100, (int) (Math.random() * 700)));
            }
        }

        if (Math.random() < 0.001) {
            boolean left = Math.random() > 0.5;
            if (left) {
                npcs.add(new Diver(100, (int) (Math.random() * 700)));
            } else {
                npcs.add(new Diver(WIDTH - 150, (int) (Math.random() * 700)));
            }
        }


        //Collision
        if (!npcs.isEmpty()) {
            List<NPC> toRemove = new LinkedList<>();
            for (NPC npc : npcs) {
                int dx = Math.abs(player.getX() - npc.getX());
                int dy = Math.abs(player.getY() - npc.getY());
                int h, w;

                if (player.getX() > npc.getX()) {
                    w = npc.getW();
                } else {
                    w = player.getW();
                }

                if (player.getY() > npc.getY()) {
                    h = npc.getH();
                } else {
                    h = player.getH();
                }

                if (dx <= w && dy <= h) {
                    if (npc.getClass().toString().equals("class model.Fish")) {
                        toRemove.add(npc);
                        score += 20;
                        player.eat();
                    } else if (npc.getClass().toString().equals("class model.UBoot")) {
                        if (player.getShield() > 0) {
                            toRemove.add(npc);
                            score += 200;
                            player.setShield(0);
                        } else {
                            getPlayer().damage(20);
                        }
                    } else if (npc.getClass().toString().equals("class model.Diver")) {
                        toRemove.add(npc);
                        score += 100;
                        player.eat();
                    } else if (npc.getClass().toString().equals("class model.Shield")) {
                        player.setShield(500);
                        toRemove.add(npc);
                        score += 50;
                    }
                }
            }
            npcs.removeAll(toRemove);
        }

        //finishes the game if plaer is dead
        if (player.getLife() <= 0) {
            finished = true;

            //new highscore? =)
            if (score > highscore) {
                try {
                    writer = new FileWriter("src/HighScore.txt");
                    writer.write(String.valueOf(score));
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
