package model;

public class Player {
    //coordinates of player
    private int x, y;
    //height and width of player
    private int h, w;

    private int dx, dy;

    private int stamina;

    //shark turned left?
    private boolean left;
    private boolean sprinting = false;
    private int shield = 0;

    private int life;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 100;
        this.w = 200;
        left = true;
        life = 1000;
        stamina = 500;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public boolean isLeft() {
        return left;
    }

    public int getLife() {
        return life;
    }

    public int getStamina() {
        return stamina;
    }

    public boolean isSprinting() {
        return sprinting;
    }

    public void setSprinting(boolean sprinting) {
        this.sprinting = sprinting;
    }

    public int getShield() {
        return shield;
    }

    public void setShield(int shield) {
        this.shield = shield;
    }

    public void move() {
        this.x += dx;
        this.y += dy;
        if (dx > 0) {
            left = false;
        } else {
            left = true;
        }
        if (sprinting) {
            stamina -= 2;
        }
    }


    public void damage(int dmg) {
        life -= dmg;
    }

    public void updateLife() {
        life--;
        if (shield > 0) {
            shield--;
        }
    }

    public void eat() {
        if (life < 500) {
            life += 500;
        } else {
            life = 1000;
        }
    }

    public void regenerate() {
        if (stamina < 500) {
            stamina++;
        }
    }




}
