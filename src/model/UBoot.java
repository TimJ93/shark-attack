package model;

public class UBoot extends NPC {
    public UBoot(int x, int y) {
        super(x, y);
        this.h = 120;
        this.w = 150;
        this.dx = 2;
        this.dy = 0;
    }

    public void update() {
        if (Math.random() < 0.009) {
            dx = (int) (Math.random() * 16 - 8);
            dy = (int) (Math.random() * 16 - 8);

        }
        if (x + dx > Model.WIDTH - w || x + dx < 0) {
            dx *= -1;
        }
        if (y + dy > Model.HEIGHT - h || y + dy < 0) {
            dy *= -1;
        }

        if (x + dx <= Model.WIDTH - w && x + dx >= 0 &&
                y + dy <= Model.HEIGHT - h && y + dy >= 0) {
            move();
        }
    }

}
