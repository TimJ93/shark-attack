import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {
    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {
        if (keycode == KeyCode.UP && model.getPlayer().getY() - 10 >= 0) {
            model.getPlayer().setDy(-2);
        }
        if (keycode == KeyCode.DOWN && model.getPlayer().getY() + 10 <= model.HEIGHT - model.getPlayer().getH()) {
            model.getPlayer().setDy(2);
        }
        if (keycode == KeyCode.LEFT && model.getPlayer().getX() - 10 >= 0) {
            model.getPlayer().setDx(-2);
        }
        if (keycode == KeyCode.RIGHT && model.getPlayer().getX() + 10 <= model.WIDTH - model.getPlayer().getW()) {
            model.getPlayer().setDx(2);
        }
        if (keycode == KeyCode.SHIFT) {
            model.getPlayer().setDx(model.getPlayer().getDx() * 3);
            model.getPlayer().setDy(model.getPlayer().getDy() * 3);
            model.getPlayer().setSprinting(true);
        }
        if (keycode == KeyCode.SPACE) {
            model.reset();
        }

    }

    public void onKeyReleased(KeyCode keycode) {
        if (keycode == KeyCode.UP || keycode == KeyCode.DOWN) {
            model.getPlayer().setDy(0);
        }

        if (keycode == KeyCode.LEFT || keycode == KeyCode.RIGHT) {
            model.getPlayer().setDx(0);
        }

        if (keycode == KeyCode.SHIFT) {
            model.getPlayer().setDx(model.getPlayer().getDx() / 3);
            model.getPlayer().setDy(model.getPlayer().getDy() / 3);
            model.getPlayer().setSprinting(false);
        }
    }
}
