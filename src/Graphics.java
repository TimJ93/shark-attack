import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Model;
import model.NPC;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {
    private GraphicsContext gc;
    private Model model;

    //loading all images
    private Image bkg = new Image(new FileInputStream("src/pictures/Background.jpg"));
    private Image hai = new Image(new FileInputStream("src/pictures/Hai.png"));
    private Image hai_right = new Image(new FileInputStream("src/pictures/Hai_rechts.png"));
    private Image clownFish = new Image(new FileInputStream("src/pictures/clownfish.png"));
    private Image clownFish_right = new Image(new FileInputStream("src/pictures/clownfish_right.png"));
    private Image submarine = new Image(new FileInputStream("src/pictures/submarine.png"));
    private Image submarine_right = new Image(new FileInputStream("src/pictures/submarine_right.png"));
    private Image diver = new Image(new FileInputStream("src/pictures/diver.png"));
    private Image diver_right = new Image(new FileInputStream("src/pictures/diver_right.png"));
    private Image shield = new Image(new FileInputStream("src/pictures/shield.png"));
    private Image endscreen = new Image(new FileInputStream("src/pictures/endscreen.gif"));

    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.drawImage(bkg, 0, 0, Model.WIDTH, Model.HEIGHT);

        gc.setFont(new Font(30));
        gc.setStroke(Color.WHITE);
        gc.strokeText("Score: " + model.getScore(), Model.WIDTH / 2 - 50, 40);

        gc.strokeText("Highscore: " + model.getHighscore(), 40, 40);

        gc.setFill(Color.RED);
        gc.fillRect(Model.WIDTH / 2 - 50, 50, model.getPlayer().getLife() * 2 / 10, 50);
        gc.setFill(Color.YELLOW);
        gc.fillRect(Model.WIDTH / 2 - 50, 110, model.getPlayer().getStamina() * 4 / 10, 50);

        if (model.getPlayer().getShield() > 0) {
            gc.drawImage(shield, model.getPlayer().getX() + 80, model.getPlayer().getY() - 50, 40, 40);
        }

        if (model.getPlayer().isLeft()) {
            gc.drawImage(hai, model.getPlayer().getX(),
                    model.getPlayer().getY(),
                    model.getPlayer().getW(), model.getPlayer().getH());
        } else {
            gc.drawImage(hai_right, model.getPlayer().getX(),
                    model.getPlayer().getY(),
                    model.getPlayer().getW(), model.getPlayer().getH());
        }
        for (NPC npc : model.getNpcs()) {
            if (npc.getClass().toString().equals("class model.UBoot")) {
                if (npc.getDx() > 0) {
                    gc.drawImage(submarine_right, npc.getX(), npc.getY(), npc.getW(), npc.getH());
                } else {
                    gc.drawImage(submarine, npc.getX(), npc.getY(), npc.getW(), npc.getH());
                }
            } else if (npc.getClass().toString().equals("class model.Fish")) {
                if (npc.getDx() > 0) {
                    gc.drawImage(clownFish_right, npc.getX(), npc.getY(), npc.getW(), npc.getH());
                } else {
                    gc.drawImage(clownFish, npc.getX(), npc.getY(), npc.getW(), npc.getH());
                }
            } else if (npc.getClass().toString().equals("class model.Diver")) {
                if (npc.getDx() > 0) {
                    gc.drawImage(diver_right, npc.getX(), npc.getY(), npc.getW(), npc.getH());
                } else {
                    gc.drawImage(diver, npc.getX(), npc.getY(), npc.getW(), npc.getH());
                }
            } else if (npc.getClass().toString().equals("class model.Shield")) {
                gc.drawImage(shield, npc.getX(), npc.getY(), npc.getW(), npc.getH());
            }
        }

        //endscreen if game is finished
        if (model.isFinished()) {
            gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
            gc.drawImage(endscreen, 0, 0, Model.WIDTH, Model.HEIGHT);
            gc.setFill(Color.BLACK);
            gc.fillText("Score: " + model.getScore(), Model.WIDTH / 2 - 100, 3 * Model.HEIGHT / 4);
            if (model.getScore() > model.getHighscore()) {
                gc.fillText("NEW HIGHSCORE !!!", Model.WIDTH / 2 - 100, 3 * Model.HEIGHT / 4 + 50);
            } else {
                gc.fillText("Highscore: " + model.getHighscore(), Model.WIDTH / 2 - 100, 3 * Model.HEIGHT / 4 + 50);
            }
            gc.fillText("Press Space to Restart", Model.WIDTH / 2 - 150, 3 * Model.HEIGHT / 4 + 150);
        }
    }
}
